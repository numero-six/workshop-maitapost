% La ligne suivante indique à Metapost de stocker chaque figure produite dans un dossier nommé ‘svg’
outputtemplate := "svg/%c.svg";
% Pour plus de comodité dans la manipulation des fichiers on précise ici qu'on aimerait avoir des fichiers vectoriels, au format svg en sortie
outputformat := "svg";

def variables=
	% On définit ici la valeur de l'unité de base
	u = 9pt;
	% On peut faire varier le coefficient de cette valeur en x et en y
	ux = 1u;
	uy = 1u;

	deg = 20;

	% Les 3 lignes suivantes permettent d'afficher (1) ou d'éteindre (0) l'affichage des éléments de la grille de dessin
	grid = 0;
	hints = 0;
	dot_label = 0;
	height := 8;
	baseline := 0;
	xHeight := 4;
	ascHeight := 8;
	descHeight := -2;
	capHeight := 6;
	% Gijs: For me it would make more sense to either have all variables
	% 'expressed in' uy, or all as numbers
	% Pierre : yes
	capOvershoot := 0.15uy;
	xOvershoot := 2.1uy;
	HHeight := capHeight*uy;
	ascxHeight := 6uy;

	r := 2.25;
	rLowercase := 1.5;
	cutLine := 0.85uy;

	% L'épaisseur du trait peut être préciser en x et en y, l'inclinaison de la plume est définie avec une valeur de rotation.
	strokeX := 1u;
	strokeY := 1u;
	rotation := 45;

	transform T;
	T = identity shifted (3ux,0uy);

	%test=0;	

	slant := 0;

	transform S;
	S = identity slanted 7/9;

	varr := .6;

	% point sur un cercle à un angle de 30 degré
	rayon := 1.5;
	posY_dot_circle := rayon/2;
	posX_dot_circle := sqrt(rayon*rayon-posY_dot_circle*posY_dot_circle);
	%show(posY_dot_circle);
	%show(posX_dot_circle);


	% GRAISSES

	%maincolor = (255,255,255);

	def col =
		((red/255)*250)+((green/255)*245)+((blue/255)*230);
	enddef;

enddef;

% Les lignes ci-dessous permettent de dessiner la grille en faisant appel aux variables définies plus haut.
def beginchar(expr keycode, width, leftSpace, rightSpace)=
	beginfig(keycode);
		variables;
		pickup pencircle scaled .2;

		if unknown leftSpace: leftSpace  :=  1 fi;
		if unknown rightSpace: rightSpace  :=  1 fi;

		draw (- leftSpace * ux, (descHeight - 2) * uy) -- 
			((width + rightSpace) * ux, (descHeight - 2) * uy) --
			((width + rightSpace) * ux, (ascHeight + 2) * uy) -- 
			(- leftSpace * ux, (ascHeight + 2) * uy) -- 
			cycle scaled 0 withcolor red;

		if grid = 1:
			defaultscale := .2;
			for i=0 upto width:
				draw (i*ux, ascHeight*uy) -- (i*ux, descHeight*uy) withcolor .3white;
			endfor;
			for i=descHeight upto (ascHeight):
				draw (width*ux, i*uy) -- (0*ux, i*uy) withcolor .3white;
			endfor;
		fi;

		pickup pencircle scaled 1;

		if hints = 1:
			% draw (0 * ux, (xHeight * uy) + os_x) -- (width * ux, (xHeight * uy) + os_x)  withcolor green;
			% draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);

			draw (0 * ux, capHeight * uy) -- (width * ux, capHeight * uy)  withcolor (green + blue);
			draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);
			draw (0 * ux, ascHeight * uy) -- (width * ux, ascHeight * uy)  withcolor (green + blue);
			draw (0 * ux, descHeight * uy) -- (width * ux, descHeight * uy)  withcolor (green + blue);
			draw (0 * ux, baseline * uy) -- (width * ux, baseline * uy)  withcolor green;
		fi;
		linejoin := mitered;
		linecap := squared;


		pickup pencircle xscaled strokeX yscaled strokeY rotated rotation;

	enddef;

	% Pour s'y retrouver on peut matérialiser et numéroter les points du tracé.
	def endchar(expr lenDots)=
	if dot_label = 1:
		defaultscale := 3;
		for i=1 upto lenDots:
			dotlabels.urt([i]) withcolor blue;
		endfor;
	fi;
endfig;
enddef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODULES 

%La typo as une hauteur d'x de 11pixels, ascendantes à 15 et descendantes à -4

def module_test(expr trX, cur,posX_end)=
	variables;
	path p;
	p = (0ux,0uy)--(0ux,1uy)--(1ux,1uy)--(1ux,0uy)--cycle;
	if slant = 0 :
		draw p;
		fill p withcolor col;
	else :
		draw p transformed S;
	fi
enddef;

def sample_module(expr trX, cur,posX_end)=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	a = (ux,11uy)shifted(0ux,0uy)--(ux,11uy)shifted(0ux,0uy);
	b = (ux,10uy)shifted(0ux,0uy)--(ux,10uy)shifted(0ux,0uy);
	c = (ux,9uy) shifted(0ux,0uy)--(ux,9uy) shifted(0ux,0uy);
	d = (ux,8uy) shifted(0ux,0uy)--(ux,8uy) shifted(0ux,0uy);
	e = (ux,7uy) shifted(0ux,0uy)--(ux,7uy) shifted(0ux,0uy);
	f = (ux,6uy) shifted(0ux,0uy)--(ux,6uy) shifted(0ux,0uy);
	g = (ux,5uy) shifted(0ux,0uy)--(ux,5uy) shifted(0ux,0uy);
	h = (ux,4uy) shifted(0ux,0uy)--(ux,4uy) shifted(0ux,0uy);
	i = (ux,3uy) shifted(0ux,0uy)--(ux,3uy) shifted(0ux,0uy);
	j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(0ux,0uy);
	k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def boucle_bas_gauche(expr trX, cur,posX_end)=
	variables;
	%path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;
	%a = (ux,11uy)--(ux,11uy);
	b = (2ux,10uy)--(2ux,10uy);
	c = (1ux,9uy)--(2ux,9uy);
	d = (1ux,8uy)--(1ux,8uy);
	e = (0ux,7uy)--(1ux,7uy);
	f = (0ux,6uy)--(2ux,6uy);
	g = (0ux,5uy)--(2ux,5uy);
	h = (1ux,4uy)--(3ux,4uy);
	i = (1ux,3uy)--(5ux,3uy);
	j = (2ux,2uy)--(8ux,2uy);
	k = (4ux,1uy)--(6ux,1uy);
	l = (8ux,3uy)--(9ux,3uy);
	if slant = 0 :
		%draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		%fill p withcolor col;
	else :
		draw p transformed S;
	fi
enddef;

def boucle_haut_droit(expr trX, cur, posX_end)=

	variables;

	def shift =
		(0ux,0uy) ;
	enddef;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	%path j;
	%path k;
	%path l;
	%path m;
	%path n;
	
	a = (4ux,11uy)shifted(0ux,0uy)--(6ux,11uy)shifted(shift);
	b = (2ux,10uy)shifted(0ux,0uy)--(8ux,10uy)shifted(shift);
	c = (5ux,9uy) shifted(0ux,0uy)--(9ux,9uy) shifted(shift);
	d = (7ux,8uy) shifted(0ux,0uy)--(9ux,8uy) shifted(shift);
	e = (8ux,7uy) shifted(0ux,0uy)--(10ux,7uy) shifted(shift);
	f = (8ux,6uy) shifted(0ux,0uy)--(10ux,6uy) shifted(shift);
	g = (9ux,5uy) shifted(0ux,0uy)--(10ux,5uy) shifted(shift);
	h = (9ux,4uy) shifted(0ux,0uy)--(9ux,4uy) shifted(shift);
	i = (9ux,3uy) shifted(0ux,0uy)--(9ux,3uy) shifted(shift);
	%j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(shift);
	%k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(shift);
	%l = (2ux,0uy) shifted(0ux,3uy)--(3ux,0uy) shifted(shift); 
	%m = (2ux,-1uy)shifted(0ux,3uy)--(3ux,-1uy)shifted(shift);
	%n = (2ux,-2uy)shifted(0ux,3uy)--(2ux,-2uy)shifted(shift);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		%draw j;
		%draw k;
		%draw l;
		%draw m;
		%draw n;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def fut_n(expr trX, cur, posX_end)=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	a = (3ux,11uy)--(3ux,11uy);
	b = (2ux,10uy)--(3ux,10uy);
	c = (1ux,9uy)--(3ux,9uy);
	d = (2ux,8uy)--(3ux,8uy);
	e = (2ux,7uy)--(3ux,7uy);
	f = (2ux,6uy)--(3ux,6uy);
	g = (2ux,5uy)--(3ux,5uy);
	h = (2ux,4uy)--(3ux,4uy);
	i = (2ux,3uy)--(3ux,3uy);
	j = (2ux,2uy)--(3ux,2uy);
	k = (2ux,1uy)--(2ux,1uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def fut_descendant(expr trX, cur, posX_end)=

	variables;

	def shift =
		(-2ux,0uy) ;
	enddef;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;
	path m;
	path n;
	
	a = (3ux,11uy)shifted(-2ux,0uy)--(3ux,11uy)shifted(shift);
	b = (2ux,10uy)shifted(-2ux,0uy)--(3ux,10uy)shifted(shift);
	c = (1ux,9uy)shifted (-2ux,0uy)--(3ux,9uy) shifted(shift);
	d = (2ux,8uy)shifted (-2ux,0uy)--(3ux,8uy) shifted(shift);
	e = (2ux,7uy)shifted (-2ux,0uy)--(3ux,7uy) shifted(shift);
	f = (2ux,6uy)shifted (-2ux,0uy)--(3ux,6uy) shifted(shift);
	g = (2ux,5uy)shifted (-2ux,0uy)--(3ux,5uy) shifted(shift);
	h = (2ux,4uy)shifted (-2ux,0uy)--(3ux,4uy) shifted(shift);
	i = (2ux,3uy)shifted (-2ux,0uy)--(3ux,3uy) shifted(shift);
	j = (2ux,2uy)shifted (-2ux,0uy)--(3ux,2uy) shifted(shift);
	k = (2ux,1uy)shifted (-2ux,0uy)--(3ux,1uy) shifted(shift);
	l = (2ux,0uy)shifted (-2ux,0uy)--(3ux,0uy) shifted(shift); 
	m = (2ux,-1uy)shifted(-2ux,0uy)--(3ux,-1uy)shifted(shift);
	n = (2ux,-2uy)shifted(-2ux,0uy)--(2ux,-2uy)shifted(shift);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		draw m;
		draw n;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def fut_ascendant(expr trX, cur, posX_end)=

	variables;

	def shift =
		(0ux,3uy) ;
	enddef;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;
	path m;
	path n;
	
	a = (3ux,11uy)shifted(0ux,3uy)--(3ux,11uy)shifted(shift);
	b = (2ux,10uy)shifted(0ux,3uy)--(3ux,10uy)shifted(shift);
	c = (1ux,9uy)shifted(0ux,3uy)--(3ux,9uy)shifted(shift);
	d = (2ux,8uy)shifted(0ux,3uy)--(3ux,8uy)shifted(shift);
	e = (2ux,7uy)shifted(0ux,3uy)--(3ux,7uy)shifted(shift);
	f = (2ux,6uy)shifted(0ux,3uy)--(3ux,6uy)shifted(shift);
	g = (2ux,5uy)shifted(0ux,3uy)--(3ux,5uy)shifted(shift);
	h = (2ux,4uy)shifted(0ux,3uy)--(3ux,4uy)shifted(shift);
	i = (2ux,3uy)shifted(0ux,3uy)--(3ux,3uy)shifted(shift);
	j = (2ux,2uy)shifted(0ux,3uy)--(3ux,2uy)shifted(shift);
	k = (2ux,1uy)shifted(0ux,3uy)--(3ux,1uy)shifted(shift);
	l = (2ux,0uy)shifted(0ux,3uy)--(3ux,0uy)shifted(shift); 
	m = (2ux,-1uy)shifted(0ux,3uy)--(3ux,-1uy)shifted(shift);
	n = (2ux,-2uy)shifted(0ux,3uy)--(2ux,-2uy)shifted(shift);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		draw m;
		draw n;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def anse_n(expr trX, cur, posX_end)=

	variables;

	def shift =
		(0ux,0uy) ;
	enddef;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	%path l;
	%path m;
	%path n;
	
	a = (3ux,11uy)shifted(0ux,0uy)--(3ux,11uy)shifted(shift);
	b = (2ux,10uy)shifted(0ux,0uy)--(3ux,10uy)shifted(shift);
	c = (1ux,9uy) shifted(0ux,0uy)--(3ux,9uy) shifted(shift);
	d = (2ux,8uy) shifted(0ux,0uy)--(3ux,8uy) shifted(shift);
	e = (2ux,7uy) shifted(0ux,0uy)--(3ux,7uy) shifted(shift);
	f = (2ux,6uy) shifted(0ux,0uy)--(3ux,6uy) shifted(shift);
	g = (2ux,5uy) shifted(0ux,0uy)--(3ux,5uy) shifted(shift);
	h = (2ux,4uy) shifted(0ux,0uy)--(3ux,4uy) shifted(shift);
	i = (2ux,3uy) shifted(0ux,0uy)--(3ux,3uy) shifted(shift);
	j = (2ux,2uy) shifted(0ux,0uy)--(3ux,2uy) shifted(shift);
	k = (2ux,1uy) shifted(0ux,0uy)--(3ux,1uy) shifted(shift);
	%l = (2ux,0uy) shifted(0ux,3uy)--(3ux,0uy) shifted(shift); 
	%m = (2ux,-1uy)shifted(0ux,3uy)--(3ux,-1uy)shifted(shift);
	%n = (2ux,-2uy)shifted(0ux,3uy)--(2ux,-2uy)shifted(shift);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%draw l;
		%draw m;
		%draw n;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def fut_u(expr trX, cur, posX_end)=

	variables;

	def shift =
		(7ux,0uy) ;
	enddef;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	a = (3ux,11uy)shifted(7ux,0uy)--(3ux,11uy)shifted(7ux,0uy);
	b = (2ux,10uy)shifted(7ux,0uy)--(3ux,10uy)shifted(7ux,0uy);
	c = (2ux,9uy) shifted(7ux,0uy)--(3ux,9uy) shifted(7ux,0uy);
	d = (2ux,8uy) shifted(7ux,0uy)--(3ux,8uy) shifted(7ux,0uy);
	e = (2ux,7uy) shifted(7ux,0uy)--(3ux,7uy) shifted(7ux,0uy);
	f = (2ux,6uy) shifted(7ux,0uy)--(3ux,6uy) shifted(7ux,0uy);
	g = (2ux,5uy) shifted(7ux,0uy)--(3ux,5uy) shifted(7ux,0uy);
	h = (2ux,4uy) shifted(7ux,0uy)--(3ux,4uy) shifted(7ux,0uy);
	i = (2ux,3uy) shifted(7ux,0uy)--(3ux,3uy) shifted(7ux,0uy);
	j = (2ux,2uy) shifted(7ux,0uy)--(3ux,2uy) shifted(7ux,0uy);
	k = (2ux,1uy) shifted(7ux,0uy)--(2ux,1uy) shifted(7ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi

enddef;

def barre_t(expr trX, cur,posX_end)=

	variables;

	path a;
	path b;
	path c;
	path d;
	%path e;
	%path f;
	%path g;
	%path h;
	%path i;
	%path j;
	%path k;

	a = (2ux,11uy)shifted(-1ux,0uy)--(9ux,11uy)shifted(-1ux,0uy);
	b = (1ux,10uy)shifted(-1ux,0uy)--(10ux,10uy)shifted(-1ux,0uy);
	c = (0ux,9uy) shifted(-1ux,0uy)--(0ux,9uy) shifted(-1ux,0uy);
	d = (11ux,11uy) shifted(-1ux,0uy)--(11ux,11uy) shifted(-1ux,0uy);
	%e = (ux,7uy) shifted(0ux,0uy)--(ux,7uy) shifted(0ux,0uy);
	%f = (ux,6uy) shifted(0ux,0uy)--(ux,6uy) shifted(0ux,0uy);
	%g = (ux,5uy) shifted(0ux,0uy)--(ux,5uy) shifted(0ux,0uy);
	%h = (ux,4uy) shifted(0ux,0uy)--(ux,4uy) shifted(0ux,0uy);
	%i = (ux,3uy) shifted(0ux,0uy)--(ux,3uy) shifted(0ux,0uy);
	%j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(0ux,0uy);
	%k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		%draw e;
		%draw f;
		%draw g;
		%draw h;
		%draw i;
		%draw j;
		%draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
enddef;

def jolie_diagonale(expr trX, cur,posX_end)=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;
	path m;

	a = (2ux,11uy)shifted(0ux,0uy)--(3ux,11uy)shifted(0ux,0uy);
	b = (1ux,10uy)shifted(0ux,0uy)--(4ux,10uy)shifted(0ux,0uy);
	c = (3ux,9uy) shifted(0ux,0uy)--(4ux,9uy) shifted(0ux,0uy);
	d = (3ux,8uy) shifted(0ux,0uy)--(5ux,8uy) shifted(0ux,0uy);
	e = (4ux,7uy) shifted(0ux,0uy)--(5ux,7uy) shifted(0ux,0uy);
	f = (4ux,6uy) shifted(0ux,0uy)--(6ux,6uy) shifted(0ux,0uy);
	g = (5ux,5uy) shifted(0ux,0uy)--(6ux,5uy) shifted(0ux,0uy);
	h = (5ux,4uy) shifted(0ux,0uy)--(7ux,4uy) shifted(0ux,0uy);
	i = (6ux,3uy) shifted(0ux,0uy)--(7ux,3uy) shifted(0ux,0uy);
	j = (6ux,2uy) shifted(0ux,0uy)--(9ux,2uy) shifted(0ux,0uy);
	k = (7ux,1uy) shifted(0ux,0uy)--(8ux,1uy) shifted(0ux,0uy);
	l = (0ux,9uy) shifted(0ux,0uy)--(0ux,9uy) shifted(0ux,0uy);
	m = (10ux,3uy) shifted(0ux,0uy)--(10ux,3uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		draw m;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def reste_du_r(expr trX, cur,posX_end)=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;

	a = (1ux,11uy)shifted(4ux,0uy)--(4ux,11uy)shifted(4ux,0uy);
	b = (2ux,10uy)shifted(4ux,0uy)--(5ux,10uy)shifted(4ux,0uy);
	c = (3ux,9uy) shifted(4ux,0uy)--(5ux,9uy) shifted(4ux,0uy);
	d = (4ux,8uy) shifted(4ux,0uy)--(5ux,8uy) shifted(4ux,0uy);
	e = (3ux,7uy) shifted(4ux,0uy)--(4ux,7uy) shifted(4ux,0uy);
	f = (1ux,6uy) shifted(4ux,0uy)--(3ux,6uy) shifted(4ux,0uy);
	g = (2ux,5uy) shifted(4ux,0uy)--(4ux,5uy) shifted(4ux,0uy);
	h = (3ux,4uy) shifted(4ux,0uy)--(5ux,4uy) shifted(4ux,0uy);
	i = (4ux,3uy) shifted(4ux,0uy)--(6ux,3uy) shifted(4ux,0uy);
	j = (5ux,2uy) shifted(4ux,0uy)--(8ux,2uy) shifted(4ux,0uy);
	k = (6ux,1uy) shifted(4ux,0uy)--(7ux,1uy) shifted(4ux,0uy);
	l = (0ux,10uy) shifted(4ux,0uy)--(0ux,10uy) shifted(4ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

%def exclamation_(expr trX, cur,posX_end)=
%
%	variables;
%
%	path a;
%	path b;
%	path c;
%	path d;
%	path e;
%	path f;
%	path g;
%	path h;
%	path i;
%	path j;
%	path k;
%
%	a = (2ux,11uy)shifted(0ux,0uy)--(3ux,11uy)shifted(0ux,0uy);
%	b = (1ux,10uy)shifted(0ux,0uy)--(7ux,10uy)shifted(0ux,0uy);
%	c = (1ux,9uy) shifted(0ux,0uy)--(7ux,9uy) shifted(0ux,0uy);
%	d = (1ux,8uy) shifted(0ux,0uy)--(7ux,8uy) shifted(0ux,0uy);
%	e = (2ux,7uy) shifted(0ux,0uy)--(6ux,7uy) shifted(0ux,0uy);
%	f = (2ux,6uy) shifted(0ux,0uy)--(6ux,6uy) shifted(0ux,0uy);
%	g = (3ux,5uy) shifted(0ux,0uy)--(5ux,5uy) shifted(0ux,0uy);
%	h = (4ux,4uy) shifted(0ux,0uy)--(4ux,4uy) shifted(0ux,0uy);
%	i = (5ux,11uy) shifted(0ux,0uy)--(6ux,11uy) shifted(0ux,0uy);
%	j = (0ux,2uy) shifted(3.5ux,0uy)--(1ux,2uy) shifted(3.5ux,0uy);
%	k = (0ux,1uy) shifted(3.5ux,0uy)--(1ux,1uy) shifted(3.5ux,0uy);
%
%	if slant = 0 :
%
%		draw a;
%		draw b;
%		draw c;
%		draw d;
%		draw e;
%		draw f;
%		draw g;
%		draw h;
%		draw i;
%		draw j;
%		draw k;
%		%fill p withcolor col;
%
%	else :
%
%		draw p transformed S;
%		
%	fi
%	
%enddef;

def la_lettre_s=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;
	path m;
	path n;
	path o;
	path p;
	path q;
%interieur
	a = (2ux,11uy)shifted(0ux,0uy)--(7ux,11uy)shifted(0ux,0uy);
	b = (1ux,10uy)shifted(0ux,0uy)--(2ux,10uy)shifted(0ux,0uy);
	c = (1ux,9uy) shifted(0ux,0uy)--(2ux,9uy) shifted(0ux,0uy);
	d = (1ux,8uy) shifted(0ux,0uy)--(4ux,8uy) shifted(0ux,0uy);
	e = (2ux,7uy) shifted(0ux,0uy)--(6ux,7uy) shifted(0ux,0uy);
	f = (3ux,6uy) shifted(0ux,0uy)--(8ux,6uy) shifted(0ux,0uy);
	g = (5ux,5uy) shifted(0ux,0uy)--(9ux,5uy) shifted(0ux,0uy);
	h = (7ux,4uy) shifted(0ux,0uy)--(9ux,4uy) shifted(0ux,0uy);
	i = (8ux,3uy) shifted(0ux,0uy)--(9ux,3uy) shifted(0ux,0uy);
	j = (8ux,2uy) shifted(0ux,0uy)--(8ux,2uy) shifted(0ux,0uy);
	k = (2ux,1uy) shifted(0ux,0uy)--(7ux,1uy) shifted(0ux,0uy);
%haut
	l = (6ux,10uy)shifted(0ux,0uy)--(8ux,10uy) shifted(0ux,0uy);
	m = (7ux,9uy) shifted(0ux,0uy)--(8ux,9uy) shifted(0ux,0uy);
%bas
	n = (0ux,5uy) shifted(0ux,0uy)--(1ux,5uy) shifted(0ux,0uy);
	o = (0ux,4uy) shifted(0ux,0uy)--(1ux,4uy) shifted(0ux,0uy);
	p = (0ux,3uy) shifted(0ux,0uy)--(2ux,3uy) shifted(0ux,0uy);
	q = (1ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);



	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		draw m;
		draw n;
		draw o;
		draw p;
		draw q;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def bide_du_a=

	variables;

	%path a;
	%path b;
	%path c;
	%path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	%a = (ux,11uy)shifted(0ux,0uy)--(ux,11uy)shifted(0ux,0uy);
	%b = (ux,10uy)shifted(0ux,0uy)--(ux,10uy)shifted(0ux,0uy);
	%c = (ux,9uy) shifted(0ux,0uy)--(ux,9uy) shifted(0ux,0uy);
	%d = (ux,8uy) shifted(0ux,0uy)--(ux,8uy) shifted(0ux,0uy);
	e = (4ux,3uy) shifted(0ux,0uy)--(5ux,3uy) shifted(0ux,0uy);
	f = (3ux,6uy) shifted(0ux,0uy)--(3ux,6uy) shifted(0ux,0uy);
	g = (2ux,5uy) shifted(0ux,0uy)--(2ux,5uy) shifted(0ux,0uy);
	h = (1ux,4uy) shifted(0ux,0uy)--(2ux,4uy) shifted(0ux,0uy);
	i = (1ux,3uy) shifted(0ux,0uy)--(2ux,3uy) shifted(0ux,0uy);
	j = (1ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);
	k = (0ux,1uy) shifted(0ux,0uy)--(2ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		%draw a;
		%draw b;
		%draw c;
		%draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def boucle_haut_coupe=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	a = (4ux,11uy)shifted(0ux,0uy)--(7ux,11uy)shifted(0ux,0uy);
	b = (6ux,10uy)shifted(0ux,0uy)--(9ux,10uy)shifted(0ux,0uy);
	c = (8ux,9uy) shifted(0ux,0uy)--(9ux,9uy) shifted(0ux,0uy);
	d = (9ux,8uy) shifted(0ux,0uy)--(9ux,8uy) shifted(0ux,0uy);
	e = (3ux,10uy) shifted(0ux,0uy)--(3ux,10uy) shifted(0ux,0uy);
	%f = (ux,6uy) shifted(0ux,0uy)--(ux,6uy) shifted(0ux,0uy);
	%g = (ux,5uy) shifted(0ux,0uy)--(ux,5uy) shifted(0ux,0uy);
	%h = (ux,4uy) shifted(0ux,0uy)--(ux,4uy) shifted(0ux,0uy);
	%i = (ux,3uy) shifted(0ux,0uy)--(ux,3uy) shifted(0ux,0uy);
	%j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(0ux,0uy);
	%k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

enddef;

def barre_milieu=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	%a = (ux,11uy)shifted(0ux,0uy)--(ux,11uy)shifted(0ux,0uy);
	%b = (ux,10uy)shifted(0ux,0uy)--(ux,10uy)shifted(0ux,0uy);
	%c = (ux,9uy) shifted(0ux,0uy)--(ux,9uy) shifted(0ux,0uy);
	%d = (ux,8uy) shifted(0ux,0uy)--(ux,8uy) shifted(0ux,0uy);
	e = (2ux,7uy) shifted(0ux,0uy)--(5ux,7uy) shifted(0ux,0uy);
	f = (3ux,6uy) shifted(0ux,0uy)--(4ux,6uy) shifted(0ux,0uy);
	%g = (ux,5uy) shifted(0ux,0uy)--(ux,5uy) shifted(0ux,0uy);
	%h = (ux,4uy) shifted(0ux,0uy)--(ux,4uy) shifted(0ux,0uy);
	%i = (ux,3uy) shifted(0ux,0uy)--(ux,3uy) shifted(0ux,0uy);
	%j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(0ux,0uy);
	%k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def pied_l=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	%a = (ux,11uy)shifted(0ux,0uy)--(ux,11uy)shifted(0ux,0uy);
	%b = (ux,10uy)shifted(0ux,0uy)--(ux,10uy)shifted(0ux,0uy);
	%c = (ux,9uy) shifted(0ux,0uy)--(ux,9uy) shifted(0ux,0uy);
	%d = (ux,8uy) shifted(0ux,0uy)--(ux,8uy) shifted(0ux,0uy);
	%e = (ux,7uy) shifted(0ux,0uy)--(ux,7uy) shifted(0ux,0uy);
	%f = (ux,6uy) shifted(0ux,0uy)--(ux,6uy) shifted(0ux,0uy);
	%g = (ux,5uy) shifted(0ux,0uy)--(ux,5uy) shifted(0ux,0uy);
	h = (4ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);
	i = (9ux,3uy) shifted(0ux,0uy)--(10ux,3uy) shifted(0ux,0uy);
	j = (8ux,2uy) shifted(0ux,0uy)--(10ux,2uy) shifted(0ux,0uy);
	k = (1ux,1uy) shifted(0ux,0uy)--(9ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def anse_n=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;
	path l;

	a = (7ux,11uy)shifted(0ux,0uy)--(10ux,11uy)shifted(0ux,0uy);
	b = (5ux,10uy)shifted(0ux,0uy)--(11ux,10uy)shifted(0ux,0uy);
	c = (9ux,9uy) shifted(0ux,0uy)--(12ux,9uy) shifted(0ux,0uy);
	d = (10ux,8uy) shifted(0ux,0uy)--(12ux,8uy) shifted(0ux,0uy);
	e = (11ux,7uy) shifted(0ux,0uy)--(12ux,7uy) shifted(0ux,0uy);
	f = (11ux,6uy) shifted(0ux,0uy)--(12ux,6uy) shifted(0ux,0uy);
	g = (11ux,5uy) shifted(0ux,0uy)--(12ux,5uy) shifted(0ux,0uy);
	h = (11ux,4uy) shifted(0ux,0uy)--(12ux,4uy) shifted(0ux,0uy);
	i = (11ux,3uy) shifted(0ux,0uy)--(12ux,3uy) shifted(0ux,0uy);
	j = (10ux,2uy) shifted(0ux,0uy)--(11ux,2uy) shifted(0ux,0uy);
	k = (10ux,1uy) shifted(0ux,0uy)--(10ux,1uy) shifted(0ux,0uy);
	l = (4ux,9uy) shifted(0ux,0uy)--(4ux,9uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;
		draw l;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def boucle_ascendante_d=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	%path i;
	%path j;
	%path k;
	path l;
	path m;
	path n;
	path o;

	l = (3ux,15uy) shifted(-1ux,0uy)--(6ux,15uy)  shifted(-1ux,0uy);
	m = (2ux,14uy) shifted(-1ux,0uy)--(8ux,14uy)  shifted(-1ux,0uy);
	n = (5ux,13uy) shifted(-1ux,0uy)--(9ux,13uy)  shifted(-1ux,0uy);
	o = (7ux,12uy) shifted(-1ux,0uy)--(10ux,12uy) shifted(-1ux,0uy);
	a = (8ux,11uy)shifted(-1ux,0uy)--(11ux,11uy)shifted(-1ux,0uy);
	b = (9ux,10uy)shifted(-1ux,0uy)--(11ux,10uy)shifted(-1ux,0uy);
	c = (10ux,9uy)shifted(-1ux,0uy)--(12ux,9uy) shifted(-1ux,0uy);
	d = (10ux,8uy)shifted(-1ux,0uy)--(12ux,8uy) shifted(-1ux,0uy);
	e = (11ux,7uy)shifted(-1ux,0uy)--(12ux,7uy) shifted(-1ux,0uy);
	f = (11ux,6uy)shifted(-1ux,0uy)--(12ux,6uy) shifted(-1ux,0uy);
	g = (11ux,5uy)shifted(-1ux,0uy)--(11ux,5uy) shifted(-1ux,0uy);
	h = (10ux,4uy)shifted(-1ux,0uy)--(11ux,4uy) shifted(-1ux,0uy);
	%i = (ux,3uy) shifted(0ux,0uy)--(ux,3uy) shifted(0ux,0uy);
	%j = (ux,2uy) shifted(0ux,0uy)--(ux,2uy) shifted(0ux,0uy);
	%k = (ux,1uy) shifted(0ux,0uy)--(ux,1uy) shifted(0ux,0uy);

	if slant = 0 :

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		%draw i;
		%draw j;
		%draw k;
		draw l;
		draw m;
		draw n;
		draw o;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def queue_qg=

	variables;

	%path l;
	%path m;
	%path n;
	%path o;

	%path a;
	%path b;
	%path c;
	%path d;
	%path e;
	%path f;
	%path g;
	%path h;
	%path i;
	%path j;
	path k;

	path p;
	path q;
	path r;
	path s;

	%l = (3ux,15uy) shifted(-1ux,0uy)--(6ux,15uy)  shifted(-1ux,0uy);
	%m = (2ux,14uy) shifted(-1ux,0uy)--(8ux,14uy)  shifted(-1ux,0uy);
	%n = (5ux,13uy) shifted(-1ux,0uy)--(9ux,13uy)  shifted(-1ux,0uy);
	%o = (7ux,12uy) shifted(-1ux,0uy)--(10ux,12uy) shifted(-1ux,0uy);

	%a = (8ux,11uy)shifted(-1ux,0uy)--(11ux,11uy)shifted(-1ux,0uy);
	%b = (9ux,10uy)shifted(-1ux,0uy)--(11ux,10uy)shifted(-1ux,0uy);
	%c = (10ux,9uy)shifted(-1ux,0uy)--(12ux,9uy) shifted(-1ux,0uy);
	%d = (10ux,8uy)shifted(-1ux,0uy)--(12ux,8uy) shifted(-1ux,0uy);
	%e = (11ux,7uy)shifted(-1ux,0uy)--(12ux,7uy) shifted(-1ux,0uy);
	%f = (11ux,6uy)shifted(-1ux,0uy)--(12ux,6uy) shifted(-1ux,0uy);
	%g = (11ux,5uy)shifted(-1ux,0uy)--(11ux,5uy) shifted(-1ux,0uy);
	%h = (10ux,4uy)shifted(-1ux,0uy)--(11ux,4uy) shifted(-1ux,0uy);
	i = (9ux,2uy) shifted(0ux,0uy)--(9ux,2uy) shifted(0ux,0uy);
	j = (8ux,1uy) shifted(0ux,0uy)--(8ux,1uy) shifted(0ux,0uy);
	k = (7ux,0uy) shifted(0ux,0uy)--(7ux,0uy) shifted(0ux,0uy);
	p = (6ux,-1uy)shifted(0ux,0uy)--(6ux,-1uy)shifted(0ux,0uy);
	q = (1ux,-2uy)shifted(0ux,0uy)--(5ux,-2uy)shifted(0ux,0uy);
	r = (2ux,-3uy)shifted(0ux,0uy)--(4ux,-3uy)shifted(0ux,0uy);
	s = (2ux,-1uy)shifted(0ux,0uy)--(3ux,-1uy)shifted(0ux,0uy);

	if slant = 0 :

		%draw a;
		%draw b;
		%draw c;
		%draw d;
		%draw e;
		%draw f;
		%draw g;
		%draw h;
		draw i;
		draw j;
		draw k;
		%draw l;
		%draw m;
		%draw n;
		%draw o;
		draw p;
		draw q;
		draw r;
		draw s;
		%fill p withcolor col;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def quote_=

	variables;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;

	a = (2ux,11uy)shifted(0ux,0uy)--(3ux,11uy)shifted(0ux,0uy);
	b = (1ux,10uy)shifted(0ux,0uy)--(7ux,10uy)shifted(0ux,0uy);
	c = (1ux,9uy) shifted(0ux,0uy)--(7ux,9uy) shifted(0ux,0uy);
	d = (1ux,8uy) shifted(0ux,0uy)--(7ux,8uy) shifted(0ux,0uy);
	e = (2ux,7uy) shifted(0ux,0uy)--(6ux,7uy) shifted(0ux,0uy);
	f = (2ux,6uy) shifted(0ux,0uy)--(6ux,6uy) shifted(0ux,0uy);
	g = (3ux,5uy) shifted(0ux,0uy)--(5ux,5uy) shifted(0ux,0uy);
	h = (4ux,4uy) shifted(0ux,0uy)--(4ux,4uy) shifted(0ux,0uy);
	i = (5ux,11uy) shifted(0ux,0uy)--(6ux,11uy) shifted(0ux,0uy);

	draw a;
	draw b;
	draw c;
	draw d;
	draw e;
	draw f;
	draw g;
	draw h;
	draw i;

enddef;

def dot_=

	variables;

	path i;
	path j;
	path k;

	i = (5ux,11uy) shifted(0ux,0uy)--(6ux,11uy) shifted(0ux,0uy);
	j = (0ux,2uy) shifted(3.5ux,0uy)--(1ux,2uy) shifted(3.5ux,0uy);
	k = (0ux,1uy) shifted(3.5ux,0uy)--(1ux,1uy) shifted(3.5ux,0uy);

	draw i;
	draw j;
	draw k;

enddef;

def reste_du_b=

	variables;

	path l;
	path m;
	%path n;
	%path o;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	l = (4ux,10uy)shifted(0ux,0uy)--(4ux,10uy)shifted(0ux,0uy);
	m = (4ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);
	%n = (5ux,13uy) shifted(0ux,0uy)--(9ux,13uy)  shifted(0ux,0uy);
	%o = (7ux,12uy) shifted(0ux,0uy)--(10ux,12uy) shifted(0ux,0uy);

	a = (4ux,11uy)shifted(1ux,0uy)--(7ux,11uy)shifted(1ux,0uy);
	b = (5ux,10uy)shifted(1ux,0uy)--(8ux,10uy)shifted(1ux,0uy);
	c = (6ux,9uy) shifted(1ux,0uy)--(8ux,9uy) shifted(1ux,0uy);
	d = (7ux,8uy) shifted(1ux,0uy)--(8ux,8uy) shifted(1ux,0uy);
	e = (6ux,7uy) shifted(1ux,0uy)--(7ux,7uy) shifted(1ux,0uy);
	f = (5ux,6uy) shifted(1ux,0uy)--(9ux,6uy) shifted(1ux,0uy);
	g = (7ux,5uy) shifted(1ux,0uy)--(10ux,5uy) shifted(1ux,0uy);
	h = (8ux,4uy) shifted(1ux,0uy)--(10ux,4uy) shifted(1ux,0uy);
	i = (9ux,3uy) shifted(1ux,0uy)--(10ux,3uy) shifted(1ux,0uy);
	j = (8ux,2uy) shifted(1ux,0uy)--(10ux,2uy) shifted(1ux,0uy);
	k = (3ux,1uy) shifted(1ux,0uy)--(9ux,1uy) shifted(1ux,0uy);

	if slant = 0 :

		draw l;
		draw m;
		%draw n;
		%draw o;

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def envolee_vyw=

	variables;

	%path l;
	%path m;
	%path n;
	%path o;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	%l = (4ux,10uy)shifted(0ux,0uy)--(4ux,10uy)shifted(0ux,0uy);
	%m = (4ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);
	%n = (5ux,13uy) shifted(0ux,0uy)--(9ux,13uy)  shifted(0ux,0uy);
	%o = (7ux,12uy) shifted(0ux,0uy)--(10ux,12uy) shifted(0ux,0uy);

	a = (13ux,11uy)shifted(-2ux,0uy)--(14ux,11uy)shifted(-2ux,0uy);
	b = (12ux,10uy)shifted(-2ux,0uy)--(14ux,10uy)shifted(-2ux,0uy);
	c = (13ux,9uy) shifted(-2ux,0uy)--(14ux,9uy) shifted(-2ux,0uy);
	d = (13ux,8uy) shifted(-2ux,0uy)--(14ux,8uy) shifted(-2ux,0uy);
	e = (12ux,7uy) shifted(-2ux,0uy)--(13ux,7uy) shifted(-2ux,0uy);
	f = (12ux,6uy) shifted(-2ux,0uy)--(13ux,6uy) shifted(-2ux,0uy);
	g = (11ux,5uy) shifted(-2ux,0uy)--(12ux,5uy) shifted(-2ux,0uy);
	h = (11ux,4uy) shifted(-2ux,0uy)--(11ux,4uy) shifted(-2ux,0uy);
	i = (10ux,3uy) shifted(-2ux,0uy)--(10ux,3uy) shifted(-2ux,0uy);
	%j = (8ux,2uy) shifted(1ux,0uy)--(10ux,2uy) shifted(1ux,0uy);
	%k = (3ux,1uy) shifted(1ux,0uy)--(9ux,1uy) shifted(1ux,0uy);

	if slant = 0 :

		%draw l;
		%draw m;
		%draw n;
		%draw o;

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		%draw j;
		%draw k;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def queue_j=

	variables;

	%path l;
	%path m;
	%path n;
	%path o;

	path a;
	path b;
	path c;
	path d;
	%path e;
	%path f;
	%path g;
	%path h;
	%path i;
	%path j;
	%path k;

	%l = (4ux,10uy)shifted(0ux,0uy)--(4ux,10uy)shifted(0ux,0uy);
	%m = (4ux,2uy) shifted(0ux,0uy)--(4ux,2uy) shifted(0ux,0uy);
	%n = (5ux,13uy) shifted(0ux,0uy)--(9ux,13uy)  shifted(0ux,0uy);
	%o = (7ux,12uy) shifted(0ux,0uy)--(10ux,12uy) shifted(0ux,0uy);

	a = (3ux,1uy) shifted(0ux,0uy)--(3ux,1uy)shifted(0ux,0uy);
	b = (2ux,0uy)shifted(0ux,0uy)--(3ux,0uy)shifted(0ux,0uy);
	c = (1ux,-1uy)shifted(0ux,0uy)--(2ux,-1uy)shifted(0ux,0uy);
	d = (0ux,-2uy)shifted(0ux,0uy)--(1ux,-2uy)shifted(0ux,0uy);
	%e = (12ux,7uy) shifted(-2ux,0uy)--(13ux,7uy) shifted(-2ux,0uy);
	%f = (12ux,6uy) shifted(-2ux,0uy)--(13ux,6uy) shifted(-2ux,0uy);
	%g = (11ux,5uy) shifted(-2ux,0uy)--(12ux,5uy) shifted(-2ux,0uy);
	%h = (11ux,4uy) shifted(-2ux,0uy)--(11ux,4uy) shifted(-2ux,0uy);
	%i = (10ux,3uy) shifted(-2ux,0uy)--(10ux,3uy) shifted(-2ux,0uy);
	%j = (8ux,2uy) shifted(1ux,0uy)--(10ux,2uy) shifted(1ux,0uy);
	%k = (3ux,1uy) shifted(1ux,0uy)--(9ux,1uy) shifted(1ux,0uy);

	if slant = 0 :

		%draw l;
		%draw m;
		%draw n;
		%draw o;

		draw a;
		draw b;
		draw c;
		draw d;
		%draw e;
		%draw f;
		%draw g;
		%draw h;
		%draw i;
		%draw j;
		%draw k;

	else :

		draw p transformed S;
		
	fi
	
enddef;

def antislash=

	variables;

	path l;
	path m;
	path n;
	path o;

	path a;
	path b;
	path c;
	path d;
	path e;
	path f;
	path g;
	path h;
	path i;
	path j;
	path k;

	%l = (3ux,2uy) shifted(0ux,0uy)--(3ux,2uy) shifted(0ux,0uy);
	m = (9ux,3uy) shifted(-1ux,0uy)--(10ux,3uy)shifted(-1ux,0uy);
	n = (8ux,2uy) shifted(-1ux,0uy)--(10ux,2uy)shifted(-1ux,0uy);
	o = (0ux,1uy) shifted(-1ux,0uy)--(9ux,1uy) shifted(-1ux,0uy);

	%a = (11ux,11uy)shifted(-2ux,0uy)--(11ux,11uy)shifted(-2ux,0uy);
	%b = (10ux,10uy)shifted(-2ux,0uy)--(10ux,10uy)shifted(-2ux,0uy);
	c = (8ux,9uy)  shifted(-1ux,0uy)--(9ux,9uy)  shifted(-1ux,0uy);
	d = (7ux,8uy)  shifted(-1ux,0uy)--(7ux,8uy)  shifted(-1ux,0uy);
	e = (6ux,7uy)  shifted(-1ux,0uy)--(6ux,7uy)  shifted(-1ux,0uy);
	f = (5ux,6uy)  shifted(-1ux,0uy)--(5ux,6uy)  shifted(-1ux,0uy);
	g = (4ux,5uy)  shifted(-1ux,0uy)--(4ux,5uy)  shifted(-1ux,0uy);
	h = (3ux,4uy)  shifted(-1ux,0uy)--(3ux,4uy)  shifted(-1ux,0uy);
	i = (1ux,3uy)  shifted(-1ux,0uy)--(2ux,3uy)  shifted(-1ux,0uy);
	j = (0ux,2uy)  shifted(-1ux,0uy)--(3ux,2uy)  shifted(-1ux,0uy);
	%k = (1ux,1uy)  shifted(0ux,0uy)--(1ux,1uy)  shifted(0ux,0uy);

	if slant = 0 :

		draw l;
		draw m;
		draw n;
		draw o;

		draw a;
		draw b;
		draw c;
		draw d;
		draw e;
		draw f;
		draw g;
		draw h;
		draw i;
		draw j;
		draw k;

	else :

		draw p transformed S;
		
	fi
	
enddef;





%	 Reste à faire

%	 boucle_f;
%	 rest_du_k;
%	 gauche_du_m;
%    milieu_w;
%    autrediagonale_x;









%Old Trucs

%def fut_n(expr trX, cur,posX_end)=
%	variables;
%	path p;
%	p = (1ux,0uy)--(1ux,8uy)--(0ux,8uy)--(0ux,9uy)--(1ux,9uy)--(1ux,10uy)--(2ux,10uy)--(2ux,11uy)--(3ux,11uy)--(3ux,1uy)--(2ux,1uy)--(2ux,0uy)--cycle;
%	if slant = 0 :
%		draw p;
%		fill p withcolor col;
%	else :
%		draw p transformed S;
%	fi
%enddef;

%def anse_n(expr trX, cur,posX_end)=
%	variables;
%	path p;
%	p = (3ux,8uy)--(3ux,9uy)--(4ux,9uy)--(4ux,10uy)--(6ux,10uy)--(6ux,11uy)--(10ux,11uy)--(10ux,10uy)--(11ux,10uy)--(11ux,9uy)--(12ux,9uy)--(12ux,2uy)--(11ux,2uy)--(11ux,1uy)--(10ux,1uy)--(10ux,0uy)--(9ux,0uy)--(9ux,2uy)--(10ux,2uy)--(10ux,7uy)--(9ux,7uy)--(9ux,8uy)--(8ux,8uy)--(8ux,9uy)--(4ux,9uy)--(4ux,8uy)--cycle;
%	if slant = 0 :
%		draw p;
%		fill p withcolor col;
%	else :
%		draw p transformed S;
%	fi
%enddef;




% FIN  MODULES 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUTS 
input letters/a;
input letters/b;
input letters/c;
input letters/d;
input letters/e;
input letters/g;
input letters/h;
input letters/i;
input letters/j;
input letters/k;
input letters/l;
input letters/m;
input letters/n;
input letters/o;
input letters/p;
input letters/q;
input letters/r;
input letters/s;
input letters/t;
input letters/u;
input letters/v;
input letters/w;
input letters/x;
input letters/y;
input letters/z;
input letters/f;

input letters_caps/A;
input letters_caps/F;
input letters_caps/E;
input letters_caps/H;
input letters_caps/I;
input letters_caps/M;
input letters_caps/L;
input letters_caps/T;
input letters_caps/N;
input letters_caps/C;
input letters_caps/J;
input letters_caps/G;
input letters_caps/U;
input letters_caps/V;
input letters_caps/B;
input letters_caps/Y;
input letters_caps/P;
input letters_caps/W;
input letters_caps/R;
input letters_caps/X;
input letters_caps/K;
input letters_caps/Z;
input letters_caps/O;
input letters_caps/Q;
input letters_caps/S;
input letters_caps/D;

input numbers/1;
input numbers/2;
input numbers/3;
input numbers/4;
input numbers/5;
input numbers/7;
input numbers/9;
input numbers/8;
input numbers/6;
input numbers/0;

input other/exclamation;
input other/fullstop;
input other/comma;
input other/question;
input other/percent;
input other/slash;
input other/dollar;
input other/space;
input other/apostrophe;

% FIN INPUTS   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end;
